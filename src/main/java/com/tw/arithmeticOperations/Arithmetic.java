package com.tw.arithmeticOperations;

public class Arithmetic {
    private final int number1;
    private final int number2;

    public Arithmetic(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public int calculateSum() {
        return this.number1 + this.number2;
    }

    public int calculateDifference() {
        return this.number1 - this.number2;
    }

    public int calculateMultiplication() {
        return this.number1 * this.number2;
    }

    public int calculateDivision() {
        return this.number1 / this.number2;
    }
}
