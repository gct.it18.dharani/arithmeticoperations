package com.tw.arithmeticOperations;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class ArithmeticTest {
    @Test
    void shouldCalculateSumWhenPositiveInputsAreGiven(){
        Arithmetic arithmetic = new Arithmetic(2,3);

        int actualSum = arithmetic.calculateSum();
        int expectedSum = 5;

       // assertEquals( actualSum, expectedSum);
       assertThat( actualSum, is(equalTo(expectedSum)));
    }

    @Test
    void shouldCalculateSumWhenNegativeInputsAreGiven(){
        Arithmetic arithmetic = new Arithmetic(-2,-3);

        int actualSum = arithmetic.calculateSum();
        int expectedSum = -5;

        assertThat( actualSum, is(equalTo(expectedSum)));
    }

    @Test
    void shouldCalculateDifferenceWhenPositiveInputsAreGiven(){
        Arithmetic arithmetic = new Arithmetic(8,3);

        int actualSum = arithmetic.calculateDifference();
        int expectedSum = 5;

        assertThat( actualSum, is(equalTo(expectedSum)));
    }

    @Test
    void shouldCalculateDifferenceWhenNegativeInputsAreGiven(){
        Arithmetic arithmetic = new Arithmetic(-8,-3);

        int actualSum = arithmetic.calculateDifference();
        int expectedSum = -5;

        assertThat( actualSum, is(equalTo(expectedSum)));
    }

    @Test
    void shouldCalculateMultiplicationWhenPositiveInputsAreGiven(){
        Arithmetic arithmetic = new Arithmetic(6,7);

        int actualMultiplication = arithmetic.calculateMultiplication();
        int expectedMultiplication = 42;

        assertThat( actualMultiplication, is(equalTo(expectedMultiplication)));
    }

    @Test
    void shouldCalculateMultiplicationWhenNegativeInputsAreGiven(){
        Arithmetic arithmetic = new Arithmetic(-6,-7);

        int actualMultiplication = arithmetic.calculateMultiplication();
        int expectedMultiplication = 42;

        assertThat( actualMultiplication, is(equalTo(expectedMultiplication)));
    }

    @Test
    void shouldCalculateDivisionWhenPositiveInputsAreGiven(){
        Arithmetic arithmetic = new Arithmetic(15,5);

        double actualDivision = arithmetic.calculateDivision();
        double expectedDivision = 3;

        assertThat( actualDivision, is(equalTo(expectedDivision)));
    }

    @Test
    void shouldCalculateDivisionWhenNegativeInputsAreGiven(){
        Arithmetic arithmetic = new Arithmetic(-15,-5);

        int actualDivision = arithmetic.calculateDivision();
        int expectedDivision = 3;

        assertThat( actualDivision, is(equalTo(expectedDivision)));
    }
}
